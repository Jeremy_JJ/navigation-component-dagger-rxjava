package com.example.testapp.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.testapp.repo.models.details.Movie
import com.example.testapp.repo.models.movies.Data
import com.example.testapp.repo.models.movies.Movy
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class BaseViewModel @Inject constructor() : ViewModel() {

    val singleData = MutableLiveData<Any>()
    val movieLists = MutableLiveData<List<Movy>>()
    val movieDetails = MutableLiveData<Movie>()

}
