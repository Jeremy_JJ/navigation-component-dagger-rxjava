package com.example.testapp.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.commit
import com.example.testapp.R
import com.example.testapp.repo.ApiRepository
import com.example.testapp.ui.MainActivity
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment() {

    private val parentLayoutId = R.id.container

    @Inject
    lateinit var viewModel: BaseViewModel

    @Inject
    lateinit var repo: ApiRepository

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(getLayout(), container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        onCreateMapView(savedInstanceState)

//        if((activity as MainActivity).viewModel != null)
//        sharedManager = (activity as MainActivity).sharedManager
//        repository = (activity as MainActivity).repository

//        viewModel = (activity as MainActivity).viewModel
        initialize()
    }

    abstract fun getLayout(): Int

    abstract fun initialize()

    fun addFragment(
        fragment: BaseFragment,
        addBackStack: Boolean = true,
        @IdRes id: Int = parentLayoutId,
        tag: String = "base_fragment_tag"
    ) {
        activity?.supportFragmentManager?.commit {
            if (addBackStack) addToBackStack(fragment.hashCode().toString())
            add(id, fragment, tag)
        }
    }

    fun replaceFragment(fragment: BaseFragment, addBackStack: Boolean = false,
                        @IdRes id: Int = parentLayoutId, tag : String = "base_fragment_tag") {
        activity?.supportFragmentManager?.commit {
            if(addBackStack) addToBackStack(fragment.hashCode().toString())
            replace(id, fragment)
        }
    }

//    lateinit var hud : KProgressHUD
//
//    fun showProgressDialog(){
//        hud = KProgressHUD.create(context)
//            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
//            .setDimAmount(0.5f)
//            .setCancellable(false)
//            .show()
//    }
//
//    fun hideProgressDialog(){
//        hud.dismiss()
//    }

    fun showMessage(text: String){
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun deleteFragment(tag : String){
        val manager = activity?.supportFragmentManager

        if(manager?.fragments?.get(manager.backStackEntryCount - 1)?.tag == tag){
            manager.popBackStackImmediate()
        }else{
            manager?.fragments?.forEach {
                if(it.tag == tag){
                    manager.beginTransaction().remove(it).commit()
                }
            }
        }
    }



    fun finishFragment() {
        activity?.supportFragmentManager?.popBackStackImmediate()
    }
}

fun AppCompatActivity.initialFragment(fragment: BaseFragment) {
//    val containerId = ViewModelProviders.of(this)[BaseViewModel::class.java].parentLayoutId
    supportFragmentManager.beginTransaction().add(R.id.container, fragment).commit()
}

fun FragmentActivity.finishFragment() {
    supportFragmentManager.popBackStackImmediate()
}

