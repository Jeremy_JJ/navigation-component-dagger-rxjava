package com.example.testapp.di.module

import android.content.Context
import com.example.testapp.base.BaseViewModel
import com.example.testapp.repo.ApiInterface
import com.example.testapp.repo.ApiRepository
import com.example.testapp.utils.Constants
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module(includes = [BaseViewModelModule::class, MovieListFragmentModule::class])
internal class AppModule {

    @Singleton
    @Provides
    fun repository(apiService: ApiInterface, viewModel: BaseViewModel): ApiRepository = ApiRepository(apiService, viewModel)

    @Singleton
    @Provides
    fun retrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()

    @Provides
    fun api(retrofit: Retrofit): ApiInterface = retrofit.create(ApiInterface::class.java)

}