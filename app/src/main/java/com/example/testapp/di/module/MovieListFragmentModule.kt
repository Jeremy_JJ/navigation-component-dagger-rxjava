package com.example.testapp.di.module

import com.example.testapp.base.BaseFragment
import com.example.testapp.ui.ExoPlayerFragment
import com.example.testapp.ui.MovieDetailsFragment
import com.example.testapp.ui.MovieListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MovieListFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeMovieListFragment(): MovieListFragment

    @ContributesAndroidInjector
    abstract fun contributeMovieDetailsFragment(): MovieDetailsFragment

    @ContributesAndroidInjector
    abstract fun contributeExoPlayerFragment(): ExoPlayerFragment
}