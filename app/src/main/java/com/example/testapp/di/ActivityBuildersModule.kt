package com.example.testapp.di

import com.example.testapp.di.module.BaseViewModelModule
import com.example.testapp.di.module.MovieListFragmentModule
import com.example.testapp.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityBuildersModule {
    @ContributesAndroidInjector(modules = [BaseViewModelModule::class, MovieListFragmentModule::class])
    internal abstract fun contributeMainInjector(): MainActivity
}