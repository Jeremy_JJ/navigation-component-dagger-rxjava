package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class MovieDetailsResponse(
    @SerializedName("code")
    val code: Int?, // 401
    @SerializedName("message")
    val message: String?, // Неавторизованный пользователь!
    @SerializedName("language")
    val language: String?, // ru
    @SerializedName("subscription_status")
    val subscriptionStatus: String?, // buy
    @SerializedName("need_update")
    val needUpdate: Boolean?, // false
    @SerializedName("operator_id")
    val operatorId: Int?, // 1
    @SerializedName("traffic")
    val traffic: Boolean?, // true
    @SerializedName("data")
    val `data`: Data?
)