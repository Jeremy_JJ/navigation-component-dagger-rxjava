package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("movie")
    val movie: Movie?
)