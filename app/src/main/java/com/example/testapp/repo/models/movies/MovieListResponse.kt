package com.example.testapp.repo.models.movies


import com.google.gson.annotations.SerializedName

data class MovieListResponse(
    @SerializedName("code")
    val code: Int?, // 200
    @SerializedName("message")
    val message: String?, // Ок!
    @SerializedName("language")
    val language: String?, // ru
    @SerializedName("subscription_status")
    val subscriptionStatus: String?, // buy
    @SerializedName("need_update")
    val needUpdate: Boolean?, // false
    @SerializedName("operator_id")
    val operatorId: Int?, // 1
    @SerializedName("traffic")
    val traffic: Boolean?, // true
    @SerializedName("data")
    val `data`: Data?
)