package com.example.testapp.repo

import android.util.Log
import android.widget.Toast
import com.example.testapp.base.BaseViewModel
import com.example.testapp.repo.models.details.MovieDetailsResponse
import com.example.testapp.repo.models.movies.MovieListResponse
import com.example.testapp.utils.Constants
import com.google.gson.GsonBuilder
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject

class ApiRepository @Inject constructor(
    val api: ApiInterface,
    val viewModel: BaseViewModel
) {

    fun getAllMovies(k: Int = 0) {
        val d = api.getMoviesList(k, Constants.TOKEN)
            .observeAndSubscribe()
            .subscribe(object : DisposableSingleObserver<MovieListResponse>() {
                override fun onSuccess(t: MovieListResponse) {
                    viewModel.movieLists.value = t.data?.movies
                }

                override fun onError(e: Throwable) {

                }
            })
    }

    fun getMovieDetails(id: Int) {
        val d = api.getMovieDetails(id, Constants.TOKEN)
            .observeAndSubscribe()
            .subscribe(object : DisposableSingleObserver<MovieDetailsResponse>() {
                override fun onSuccess(t: MovieDetailsResponse) {
                    viewModel.movieDetails.value = t.data?.movie
                }

                override fun onError(e: Throwable) {

                }
            })
    }
}

fun <T> Single<T>.observeAndSubscribe() =
    subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())