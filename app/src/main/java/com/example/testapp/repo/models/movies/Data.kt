package com.example.testapp.repo.models.movies


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("items_per_page")
    val itemsPerPage: Int?, // 18
    @SerializedName("total_items")
    val totalItems: Int?, // 22573
    @SerializedName("movies")
    val movies: List<Movy>?
)