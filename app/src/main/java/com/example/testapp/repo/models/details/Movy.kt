package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Movy(
    @SerializedName("id")
    val id: Int?, // 11195
    @SerializedName("title")
    val title: String?, // Парень-каратист 2
    @SerializedName("year")
    val year: Int?, // 1986
    @SerializedName("rates")
    val rates: RatesX?,
    @SerializedName("params")
    val params: ParamsX?,
    @SerializedName("files")
    val files: FilesX?
)