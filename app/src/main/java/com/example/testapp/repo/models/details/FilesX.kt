package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class FilesX(
    @SerializedName("poster_url")
    val posterUrl: String? // https://files.itv.uz/uploads/content/poster/2018/12/08/0c80a38b87a2791e542ed2f924d8c053-q-350x501.jpeg
)