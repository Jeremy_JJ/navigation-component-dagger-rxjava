package com.example.testapp.repo.models.movies


import com.google.gson.annotations.SerializedName

data class Movy(
    @SerializedName("id")
    val id: Int?, // 42942
    @SerializedName("title")
    val title: String?, // Shikast
    @SerializedName("year")
    val year: Int?, // 2017
    @SerializedName("rates")
    val rates: Rates?,
    @SerializedName("params")
    val params: Params?,
    @SerializedName("files")
    val files: Files?
)