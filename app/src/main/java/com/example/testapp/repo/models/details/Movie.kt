package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Movie(
    @SerializedName("id")
    val id: Int?, // 1
    @SerializedName("module_id")
    val moduleId: Int?, // 2
    @SerializedName("payment_type")
    val paymentType: String?, // tariff
    @SerializedName("year")
    val year: Int?, // 2012
    @SerializedName("title")
    val title: String?, // Мужчина нарасхват
    @SerializedName("title_eng")
    val titleEng: String?, // Playing for Keeps
    @SerializedName("description")
    val description: String?, // <p>Звезда футбола и просто шикарный мужчина по воле случая становится тренером детской футбольной команды. С этого момента для своих подопечных и их обольстительных мамочек он &#151; «Мужчина нарасхват»&#133;</p>
    @SerializedName("countries_str")
    val countriesStr: String?, // США
    @SerializedName("genres_str")
    val genresStr: String?, // Комедия, Мелодрама
    @SerializedName("rates")
    val rates: Rates?,
    @SerializedName("params")
    val params: Params?,
    @SerializedName("files")
    val files: Files?,
    @SerializedName("directors")
    val directors: List<Director>?,
    @SerializedName("scenarists")
    val scenarists: List<Scenarist>?,
    @SerializedName("producers")
    val producers: List<Producer>?,
    @SerializedName("actors")
    val actors: List<Actor>?,
    @SerializedName("movies")
    val movies: List<Movy>?
)