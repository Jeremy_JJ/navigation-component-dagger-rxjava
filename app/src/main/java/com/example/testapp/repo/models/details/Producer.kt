package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Producer(
    @SerializedName("id")
    val id: Int?, // 4200
    @SerializedName("name")
    val name: String?, // Джерард Батлер
    @SerializedName("photo_url")
    val photoUrl: String? // https://files.itv.uz/uploads/people/2019/08/24//069df6ea397c6c92491738f8740db489-q-170x225.jpeg
)