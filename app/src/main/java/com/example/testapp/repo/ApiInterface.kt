package com.example.testapp.repo

import com.example.testapp.repo.models.movies.MovieListResponse
import com.example.testapp.repo.models.details.MovieDetailsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {

    @GET("/api/content/main/2/list")
    fun getMoviesList(
        @Query("page") page: Int,
        @Query("user") user: String
    ): Single<MovieListResponse>

    @GET("api/content/main/2/show/{id}")
    fun getMovieDetails(
        @Path("id") movieId: Int,
        @Query("user") user: String
    ) : Single<MovieDetailsResponse>
}