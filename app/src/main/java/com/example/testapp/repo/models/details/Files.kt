package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Files(
    @SerializedName("poster_url")
    val posterUrl: String?, // https://files.itv.uz/uploads/content/poster/2018/12/10/81d06af9804c4864206bb6968f9ff4ef-q-700x1002.jpeg
    @SerializedName("list")
    val list: List<Quality>?,
    @SerializedName("snapshots")
    val snapshots: List<Snapshots>?,
    @SerializedName("trailers")
    val trailers: List<Any>?
)