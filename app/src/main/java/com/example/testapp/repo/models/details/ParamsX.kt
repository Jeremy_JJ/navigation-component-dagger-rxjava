package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class ParamsX(
    @SerializedName("is_hd")
    val isHd: Boolean?, // false
    @SerializedName("is_3d")
    val is3d: Boolean?, // false
    @SerializedName("is_4k")
    val is4k: Boolean?, // false
    @SerializedName("is_new")
    val isNew: Boolean?, // false
    @SerializedName("is_free")
    val isFree: Boolean?, // false
    @SerializedName("is_tvshow")
    val isTvshow: Boolean? // false
)