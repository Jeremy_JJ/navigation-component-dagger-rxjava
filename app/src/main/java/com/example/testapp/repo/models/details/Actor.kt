package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Actor(
    @SerializedName("id")
    val id: Int?, // 838
    @SerializedName("name")
    val name: String?, // Джессика Бил
    @SerializedName("photo_url")
    val photoUrl: String? // https://files.itv.uz/uploads/people/2019/08/24//0130ca377b96ac3bd33a2534c078908a-q-170x225.jpeg
)