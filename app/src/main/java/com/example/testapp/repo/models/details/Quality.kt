package com.example.testapp.repo.models.details


import com.google.gson.annotations.SerializedName

data class Quality(
    @SerializedName("file_id")
    val fileId: Int?, // 25898
    @SerializedName("quality")
    val quality: String?, // sd
    @SerializedName("quality_type")
    val qualityType: Int? // 1
)