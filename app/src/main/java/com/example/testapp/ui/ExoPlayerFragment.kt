package com.example.testapp.ui

import android.app.Dialog
import android.net.Uri
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import com.example.testapp.R
import com.example.testapp.base.BaseFragment
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.source.dash.DashMediaSource
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import kotlinx.android.synthetic.main.fragment_exoplayer.*


class ExoPlayerFragment : BaseFragment(){
    private var videoSource: MediaSource? = null
    private var exoPlayerFullscreen = true
    private var fullScreenButton: FrameLayout? = null
    private var fullScreenIcon: ImageView? = null
    private var fullScreenDialog: Dialog? = null

    override fun getLayout() = R.layout.fragment_exoplayer

    override fun initialize() {

        playVideo()

        exoPlayerView.player.addListener(object : ExoPlayer.EventListener {
            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?, reason: Int) {

            }

            override fun onTracksChanged(
                trackGroups: TrackGroupArray?,
                trackSelections: TrackSelectionArray?
            ) {

            }

            override fun onLoadingChanged(isLoading: Boolean) {

            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                if (playbackState == ExoPlayer.STATE_BUFFERING){
                    progressBar.visibility = View.VISIBLE;
                } else {
                    progressBar.visibility = View.INVISIBLE;
                }
            }

            override fun onRepeatModeChanged(repeatMode: Int) {

            }

            override fun onShuffleModeEnabledChanged(shuffleModeEnabled: Boolean) {

            }

            override fun onPlayerError(error: ExoPlaybackException?) {

            }

            override fun onPositionDiscontinuity(reason: Int) {

            }

            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

            }

            override fun onSeekProcessed() {

            }

        })
    }

    private fun initExoPlayer() {

        val bandwidthMeter = DefaultBandwidthMeter()
        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)
        val trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)
        val loadControl = DefaultLoadControl()
        val player = ExoPlayerFactory.newSimpleInstance(
            DefaultRenderersFactory(requireContext()),
            trackSelector,
            loadControl
        )
        exoPlayerView.player = player

        (exoPlayerView.player as SimpleExoPlayer).prepare(videoSource)
        exoPlayerView.player.playWhenReady = true
    }

    fun playVideo() {

        var source = "https://devstreaming-cdn.apple.com/videos/streaming/examples/img_bipbop_adv_example_fmp4/master.m3u8"

        if (videoSource == null) {
            initFullscreenDialog()
            videoSource =
                buildMediaSource(Uri.parse(source))
        }


        initExoPlayer()
    }


    private fun initFullscreenDialog() {

        fullScreenDialog = object : Dialog(
            requireContext(),
            android.R.style.Theme_Black_NoTitleBar_Fullscreen
        ) {
            override fun onBackPressed() {
                exoPlayerView.player.release()
                super.onBackPressed()
            }

            override fun onStop() {
                exoPlayerView.player.release()
                super.onStop()
            }
        }
    }

    private fun buildMediaSource(uri: Uri): MediaSource {

        val userAgent = "exoplayer-lazy"

        return if (uri.lastPathSegment!!.contains("mp3") || uri.lastPathSegment!!.contains("mp4")) {
            ExtractorMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else if (uri.lastPathSegment!!.contains("m3u8")) {
            HlsMediaSource.Factory(DefaultHttpDataSourceFactory(userAgent))
                .createMediaSource(uri)
        } else {
            val dashChunkSourceFactory = DefaultDashChunkSource.Factory(
                DefaultHttpDataSourceFactory("ua", null)
            )
            val manifestDataSourceFactory = DefaultHttpDataSourceFactory(userAgent)
            DashMediaSource.Factory(dashChunkSourceFactory, manifestDataSourceFactory).createMediaSource(
                uri
            )
        }
    }

    override fun onDestroyView() {
        exoPlayerView.player.release()
        super.onDestroyView()
    }

}