package com.example.testapp.ui

import android.app.Dialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testapp.R
import com.example.testapp.base.BaseFragment
import com.example.testapp.repo.models.details.Snapshots
import com.example.testapp.utils.checkInternet
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_movie_details.*
import kotlinx.android.synthetic.main.item_snapshot.view.*

class MovieDetailsFragment(private val idMovie: Int) : BaseFragment() {
    private lateinit var adapter: Adapter

    override fun getLayout() = R.layout.fragment_movie_details

    override fun initialize() {
        if (!checkInternet(requireContext())) {
            Snackbar.make(requireView(), "Please, check internet connection", Snackbar.LENGTH_LONG)
                .show()
        }

        adapter = Adapter()

        repo.getMovieDetails(idMovie)

        close.setOnClickListener {
            finishFragment()
        }


        imageViewPlay.setOnClickListener {
            addFragment(ExoPlayerFragment())
        }

        imgMovie.setOnClickListener {
            addFragment(ExoPlayerFragment())
        }

        screenshots.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        screenshots.adapter = adapter

        viewModel.movieDetails.observe(this, {
            if (it != null) {
                layout.visibility = View.VISIBLE
                progress.visibility = View.GONE
                name.text = it.title
                if (it.titleEng != null) {
                    nameEnglish.text = it.titleEng
                } else {
                    nameEnglish.visibility = View.GONE
                }
                Glide.with(requireContext())
                    .load(it.files?.posterUrl)
                    .placeholder(R.drawable.placeholder)
                    .into(imgMovie)
                genre.text = it.genresStr
                country.text = it.countriesStr
                year.text = it.year.toString()
                desc.text = it.description
                adapter.addAll(it.files?.snapshots!!)
            }
        })
    }



    class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {
        private var items = mutableListOf<Snapshots>()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_snapshot, parent, false)
        )

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val d = items[position]
            holder.itemView.apply {
                Glide.with(context)
                    .load(d.snapshots)
                    .placeholder(R.drawable.placeholder)
                    .into(imgScreen)
            }
        }

        override fun getItemCount() = items.size

        fun addAll(data: List<Snapshots>) {
            items = data.toMutableList()
            Log.d("XXX","$items")
            notifyDataSetChanged()
        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    }
}