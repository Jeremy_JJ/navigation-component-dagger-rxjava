package com.example.testapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.testapp.R
import com.example.testapp.base.BaseFragment
import com.example.testapp.repo.models.movies.Movy
import com.example.testapp.utils.checkInternet
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_movie_list.*
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieListFragment : BaseFragment() {
    private lateinit var adapter: Adapter
    private var k = 0
    private var loading: Boolean = true

    override fun getLayout() = R.layout.fragment_movie_list

    override fun initialize() {
        if (!checkInternet(requireContext())) {
            Snackbar.make(requireView(), "Please, check internet connection", Snackbar.LENGTH_LONG)
                .show()
        }
        adapter = Adapter()
        repo.getAllMovies(0)
        viewModel.movieLists.observe(this, {
            if (it != null) {
                if (k == 0) {
                    adapter.addAll(it)
                    progress.visibility = View.GONE
                } else {
                    adapter.loadItems(it)
                }
                loading = it.isNotEmpty()
            }
        })
        val lm = GridLayoutManager(requireContext(), 3)
        list.layoutManager = lm
        list.adapter = adapter

        adapter.setItemClickListener {
            addFragment(MovieDetailsFragment(it))
        }

        list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            var pastVisiblesItems = 0
            var visibleItemCount: Int = 0
            var totalItemCount: Int = 0

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    visibleItemCount = lm.childCount
                    totalItemCount = lm.itemCount
                    pastVisiblesItems = lm.findFirstVisibleItemPosition()

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false
                            k++
                            repo.getAllMovies(k)
                        }
                    }
                }
            }
        })
    }

    class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {
        private var items = mutableListOf<Movy>()
        private var clickListener: ((Int) -> Unit)? = null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        )

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val d = items[position]
            holder.itemView.apply {
                Glide.with(context)
                    .load(d.files?.posterUrl)
                    .placeholder(R.drawable.placeholder)
                    .into(imgPoster)
                name.text = d.title
                if (d.params?.isHd!!) {
                    quality.visibility = View.VISIBLE
                    quality.text = "HD"
                } else {
                    quality.visibility = View.GONE
                }
                if (d.params.isNew!!) {
                    txtNew.visibility = View.VISIBLE
                    txtNew.text = "NEW"
                } else {
                    txtNew.visibility = View.GONE
                }
                year.text = d.year.toString()
            }

            holder.itemView.setOnClickListener {
                clickListener?.invoke(d.id!!)
            }
        }

        override fun getItemCount() = items.size

        fun addAll(data: List<Movy>) {
            items = data.toMutableList()
            notifyDataSetChanged()
        }

        fun loadItems(data: List<Movy>) {
            items.addAll(data)
            notifyDataSetChanged()
        }

        fun setItemClickListener(f: (Int) -> Unit) {
            clickListener = f
        }

        class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    }
}