package com.example.testapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.example.testapp.App
import com.example.testapp.R
import com.example.testapp.base.BaseViewModel
import com.example.testapp.base.initialFragment
import com.example.testapp.di.ViewModelProviderFactory
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory : ViewModelProviderFactory

    lateinit var viewModel : BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AndroidInjection.inject(this)
        viewModel = ViewModelProvider(this, viewModelFactory)[BaseViewModel::class.java]
        initialFragment(MovieListFragment())
    }
}